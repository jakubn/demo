package dev.nabrdalik.hentai.demo;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * print to stdout
 * if it wasn't printed in last 10 sec
 */

@Slf4j
class DemoEventListener {
    private LogPrinter printer;
    private EventRepository eventRepository;

    DemoEventListener(LogPrinter printer, EventRepository eventRepository) {
        this.printer = printer;
        this.eventRepository = eventRepository;
    }

    void eventReceived(DemoEvent event) {
        Mono.justOrEmpty(eventRepository.findById(event.name()))
                .switchIfEmpty(Mono.defer(() -> printAndSave(event)))
                .filter(eventInRepo -> event.is10sLaterThan(eventInRepo))
                .doOnNext(eventInRepo -> print(event))
                .doOnNext(e -> eventRepository.save(event))
                .block();
    }

    private void print(DemoEvent event) {
        log.debug("Printing a new event that is later than 10s; name {}", event.name());
        printer.sout(event.name());
    }

    private Mono<DemoEvent> printAndSave(DemoEvent event) {
        log.debug("Printing first event ever with that name {}", event.name());
        printer.sout(event.name());
        eventRepository.save(event);
        return Mono.empty();
    }
}

interface EventRepository {

    Optional<DemoEvent> findById(String name);

    void save(DemoEvent event);
}

class InMemoryEventRepository implements EventRepository {
    private Map<String, DemoEvent> store = new ConcurrentHashMap<>();

    @Override
    public Optional<DemoEvent> findById(String name) {
        return Optional.ofNullable(store.get(name));
    }

    @Override
    public void save(DemoEvent event) {
        store.put(event.name(), event);
    }
}

class LogPrinter {

    void sout(String somethingToPrint) {
        System.out.println(somethingToPrint);
    }
}

@Slf4j
record DemoEvent(
        String name,
        Instant timestamp) {
    boolean is10sLaterThan(@NonNull DemoEvent eventInRepo) {
        boolean isTheEventAfter10s = timestamp.isAfter(eventInRepo.timestamp
                .plusSeconds(10));
        log.debug("Is event received {} later that 10s from event in repo {}: {}", this, eventInRepo, isTheEventAfter10s);
        return isTheEventAfter10s;
    }
}
