package dev.nabrdalik.hentai.demo

import spock.lang.Specification

import java.time.Instant

class DemoEventListenerSpec extends Specification {
    private static Instant startupTime = Instant.now()
    LogPrinter printer = Mock()
    EventRepository repository = new InMemoryEventRepository()
    DemoEventListener listener = new DemoEventListener(printer, repository)
    String eventName = "demoEventName"

    def "should print event name max every 10s"() {
        when: "2 event sent in 10s"
            listener.eventReceived(demoEvent(1))
            listener.eventReceived(demoEvent(2))

        then: "name of the event is printed only once"
            1 * printer.sout(eventName)

        when: "11s have passed and another event is received"
            listener.eventReceived(demoEvent(15))

        then: "name is printed again"
            1 * printer.sout(eventName)
    }

    DemoEvent demoEvent(int secondsToAdd) {
        return new DemoEvent(eventName, startupTime.plusMillis(secondsToAdd * 1000))
    }
}
