plugins {
	java
	groovy
    id("org.springframework.boot") version "3.1.5"
    id("io.spring.dependency-management") version "1.1.3"
    id("com.gorylenko.gradle-git-properties") version "2.4.1"
}


group = "nabrdalik.dev"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_21

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
}

dependencies {
	//Spring
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("org.springframework.boot:spring-boot-starter-aop")
	annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

	//Lombok
	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")

	//DB
	runtimeOnly("org.postgresql:postgresql")

	//Tests
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.testcontainers:junit-jupiter")
	testImplementation("org.testcontainers:postgresql")
	testImplementation("org.apache.groovy:groovy")


	//Tests - spock
	testImplementation("com.athaydes:spock-reports:2.4.0-groovy-4.0")
	testImplementation(platform("org.spockframework:spock-bom:2.3-groovy-4.0"))
	testImplementation("org.spockframework:spock-core")
	testImplementation("org.spockframework:spock-spring")
	testImplementation("org.testcontainers:postgresql")
	testImplementation("org.testcontainers:spock")
}

tasks.withType<Test> {
	useJUnitPlatform()
	testLogging {
		showExceptions = true
		events("skipped", "failed")
	}
}

